#!/bin/bash
set -e

mirror_xivo="http://mirror.xivo.solutions"
update='apt-get update'
install='apt-get install --assume-yes'
download='apt-get install --assume-yes --download-only'
dcomp='/usr/bin/xivo-dcomp'

# Please update "default_lts_preinstallation" with archive installation steps when changing distribution
distribution='xivo-jabbah'
dev_lts='kuma'
repo='debian'
debian_codename='bullseye'
debian_version='11'
debian_codename_dev='bullseye'
debian_version_dev='11'
not_installable_message='This version of XiVO is not installable on 32-bit systems.'
configure_reply=false

declare -A lts_version_table=([2017.03]="Five" [2017.11]="Polaris" [2018.05]="Aldebaran" [2018.16]="Borealis" [2019.05]="Callisto" [2019.12]="Deneb" [2020.07]="Electra" [2020.18]="Freya" [2021.07]="Gaia" [2021.15]="Helios" [2022.05]="Izar" [2022.10]="Jabbah")

get_system_architecture() {
    architecture=$(uname -m)
}

check_system() {
    local version_file='/etc/debian_version'
    if [ ! -f $version_file ]; then
        echo "You must install XiVO on a Debian $debian_version (\"$debian_codename\") system"
        echo "You do not seem to be on a Debian system"
        exit 1
    else
        your_version=$(cut -d '.' -f 1 "$version_file")
    fi

    if [ "$your_version" != $debian_version ]; then
        echo "You must install XiVO on a Debian $debian_version (\"$debian_codename\") system"
        echo "You are currently on a Debian $your_version system"
        exit 1
    fi
}

add_dependencies() {
    $update
    # dirmngr and gnupg is needed by apt-key in Debian 9-10
    $install wget \
        dirmngr \
        gnupg
}

add_xivo_key() {
    wget $mirror_xivo/xivo_current.key -O - | apt-key add -
}

add_docker_key() {
    # For archive versions. From version 2019.11 docker key is installed by xivo-docker
    DOCKER_KEYRING_FILE="/etc/apt/trusted.gpg.d/download.docker.com.gpg"
    test -e ${DOCKER_KEYRING_FILE} || touch ${DOCKER_KEYRING_FILE}
    is_key_installed=$(gpg --batch --no-tty --keyring ${DOCKER_KEYRING_FILE} --list-keys --with-colons|grep -q "0EBFCD88"; echo $?)
    if [ "$is_key_installed" -eq 1 ]; then
        echo "Adding Docker GPG key..."
        curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key --keyring ${DOCKER_KEYRING_FILE} add -
    fi
}

add_docker-engine_key() {
    echo "Adding Docker Engine GPG key..."
    apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
}


add_xivo_apt_conf() {
    echo "Installing APT configuration file: /etc/apt/apt.conf.d/90xivo"

	cat > /etc/apt/apt.conf.d/90xivo <<-EOF
	Aptitude::Recommends-Important "false";
	APT::Install-Recommends "false";
	APT::Install-Suggests "false";
	EOF
}

add_mirror() {
    echo "Adding xivo mirrors"
    local mirror="deb $mirror_xivo/$repo $distribution main"
    apt_dir="/etc/apt"
    sources_list_dir="$apt_dir/sources.list.d"
    if ! grep -qr "$mirror" "$apt_dir"; then
        echo "$mirror" > $sources_list_dir/tmp-pf.sources.list
    fi
    add_xivo_key

    export DEBIAN_FRONTEND=noninteractive
    $update
    $install xivo-dist
    add_docker_key # Run after xivo-dist installation: therefore xivo-docker is installed which installs docker dependencies (among them: curl and ca-certificates)
    xivo-dist "$distribution"


    rm -f "$sources_list_dir/tmp-pf.sources.list"
    $update
}

add_pgdg_mirror() {
    $install ca-certificates lsb-release
    wget --quiet -O - https://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add -
    sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    $update
}

check_source_exists() {
    local type="$1"; shift
    local distribution="$1"; shift
    local component="$1"; shift
    local list="$1"; shift

    grep -Prq "^\s*${type}\s.+\s${distribution}\s+${component}\b" "${list}"
}

add_backports_in_sources_list() {
    local distribution=${1}; shift

    echo "Adding ${distribution}-backports source"
    if ! check_source_exists deb ${distribution}-backports main "/etc/apt/sources.list*"; then
		cat >> /etc/apt/sources.list <<-EOF

			# ${distribution}-backports, previously on backports.debian.org
			deb http://ftp.fr.debian.org/debian ${distribution}-backports main
		EOF
    fi
    if ! check_source_exists deb-src ${distribution}-backports main "/etc/apt/sources.list*"; then
		cat >> /etc/apt/sources.list <<-EOF
			deb-src http://ftp.fr.debian.org/debian ${distribution}-backports main
		EOF
    fi
}

install_dahdi_modules() {
    flavour=$(echo $(uname -r) | cut -d\- -f3)
    kernel_release=$(ls /lib/modules/ | grep ${flavour})
    for kr in ${kernel_release}; do
        ${download} dahdi-linux-modules-${kr}
        ${install} dahdi-linux-modules-${kr}
    done
}

install_rabbitmq() {
    # Install xivo-config, docker-ce, xivo-docker-components and other requirements
    ${download} xivo-docker-components
    ${install} xivo-docker-components

    xivo-dcomp pull rabbitmq
    xivo-dcomp up -d rabbitmq
}

install_xivo () {
    wget -q -O - $mirror_xivo/d-i/$debian_codename/pkg.cfg | debconf-set-selections

    kernel_release=$(uname -r)
    ${install} --purge postfix

    # Only install dahdi-linux-modules if the package dahdi-linux-dkms was not found
    dahdi_dkms_exists=$(apt-cache pkgnames | grep -q "dahdi-linux-dkms"; echo $?)
    if [ $dahdi_dkms_exists -eq 0 ]; then
      echo "DAHDI: dahdi-linux-dkms package found in repository"
    else
      echo "DAHDI: dahdi-linux-dkms package not found in repository"
      install_dahdi_modules
    fi

    if [ $do_install_rabbitmq_container -eq 1 ]; then
        install_rabbitmq
    fi

    ${download} xivo
    ${install} xivo

    # Install asterisk default moh
    ${install} asterisk-moh-opsound-g722 asterisk-moh-opsound-gsm asterisk-moh-opsound-wav

    if [ -f "${dcomp}" ]; then
        ${dcomp} pull
    fi
    xivo-service restart all

    if [ $? -eq 0 && configure_reply == false ]; then
        echo 'You must now finish the installation'
        xivo_ip=$(ip a s eth0 | grep -E 'inet.*eth0' | awk '{print $2}' | cut -d '/' -f 1 )
        echo "open https://$xivo_ip to configure XiVO"
    fi
}

check_distribution_is_32bit() {
    if [ "$distribution" = "xivo-polaris" ] || [ "$distribution" = "xivo-five" ]; then
        return 0
    else
        return 1
    fi
}

propose_polaris_installation() {
    echo $not_installable_message
    read -p 'Would you like to install XiVO Polaris [Y/n]? ' answer
    answer="${answer:-Y}"
    if [ "$answer" != 'y' -a "$answer" != 'Y' ]; then
        exit 0
    fi
    distribution="xivo-polaris"
}

check_system_is_64bit() {
    if [ "$architecture" != "x86_64" ]; then
        echo $not_installable_message
        exit 1
    fi
}

check_archive_prefix() {
    if [ "${1::5}" = "xivo-" ]; then
        echo 'Archive version must be typed without the prefix "xivo-"'
        exit 1
    fi
}

get_xivo_package_version_installed() {
    local xivo_version_installed="$(LANG='C' apt-cache policy xivo | grep Installed | grep -oE '[0-9]{2,4}\.[0-9]+(\.[0-9]+)?|1\.2\.[0-9]{1,2}' | head -n1)"
    echo $xivo_version_installed
}

postinst_actions() {
    # On Debian 9 Spawn-FCGI needs to be started at the end of installation (before webi was dockerized)
    if [ "$debian_version" \> "7" ] && [ "$(get_xivo_package_version_installed)" \< "2019.10.00" ]; then
        systemctl start spawn-fcgi
    fi
}

usage() {
    cat << EOF
    This script is used to install XiVO

    usage : $(basename $0) {-d|-a version}
        whitout arg : install production version
        -d          : install development version
        -a version  : install archived version (14.18 or later)
        -s          : pass the wizard via "variables" file

EOF
    exit 1
}

ask_before_proceeding_installation(){
    user_has_been_asked_before=1
    local option=$1
    local version=$2
    local version_prefix=${version:0:7}
    if [[ $# -eq 0 ]]; then
        echo "You are going to install production version : $distribution."
    elif [[ $version_prefix =~ ^[0-9]{4}\.[0-9]{2}+$ ]] && [[ $option == "-a" ]]; then
        echo "You are going to install xivo $(get_version_name $version_prefix) ($version)."
    elif [[ $option == "-d" ]]; then
        echo "You are going to install XiVO ${dev_lts} dev version."
        echo "To install production version, re-run the script without parameters."
    else
        echo "Be careful ! The version $option $version you choose was not recognized."
    fi

    echo "The installation will start in 5 seconds. Type Ctrl+C if you want to abort."
    for _ in $(seq 1 1 5); do
        sleep 1
        echo -n "."
    done
    echo "Go!"
    sleep 1
}

get_version_name() {
    if [[ -v lts_version_table[$1] ]]; then
        echo ${lts_version_table[$1]}
    else
        echo 'intermediate version'
    fi
}

recover_from_wrong_distrib_input(){
    if [ -f /etc/apt/sources.list.d/tmp-pf.sources.list ]; then
        rm /etc/apt/sources.list.d/tmp-pf.sources.list
    fi
}

default_lts_preinstallation() {
    check_system_is_64bit
    do_add_pgdg_mirror=1
    do_install_rabbitmq_container=0
}

source_variables_file(){
    source variables

    # Check if there's value, else, will add default
    XIVO_HOST=${XIVO_HOST:-127.0.0.1}
    LTS_NAME=${LTS_NAME:-jabbah}
    LTS_NUMBER=${LTS_NUMBER:-2022.10}
    XIVO_IP_DATA=${XIVO_IP_DATA:-127.0.0.1}
    XIVO_IP_VOIP=${XIVO_IP_VOIP:-127.0.0.1}
    XIVO_FQDN=${:XIVO_FQDN:-localhost}

    # All vars for passing the wizard oh the XiVO PBX
    ADMIN_PASSWORD${ADMIN_PASSWORD:-password}
    CONTEXT_INCALL_DID_LENGTH${CONTEXT_INCALL_DID_LENGTH:-10}
    CONTEXT_INCALL_DISPLAY_NAME${CONTEXT_INCALL_DISPLAY_NAME:-Incalls}
    CONTEXT_INCALL_NUMBER_START${CONTEXT_INCALL_NUMBER_START:-0000000000}
    CONTEXT_INCALL_NUMBER_END${CONTEXT_INCALL_NUMBER_END:-9999999999}
    CONTEXT_INTERNAL_DISPLAY_NAME${CONTEXT_INTERNAL_DISPLAY_NAME:-Default}
    CONTEXT_INTERNAL_NUMBER_START${CONTEXT_INTERNAL_NUMBER_START:-9999}
    CONTEXT_INTERNAL_NUMBER_END${CONTEXT_INTERNAL_NUMBER_END:-0000}
    CONTEXT_OUTCALL_DISPLAY_NAME${CONTEXT_OUTCALL_DISPLAY_NAME:-Outcalls}
    DEFAULT_FRENCH_CONFIGURATION${DEFAULT_FRENCH_CONFIGURATION:-true}
    ENTITY_NAME${ENTITY_NAME:-XiVO}
    LANGUAGE${LANGUAGE:-fr_FR}
    NETWORK_DOMAIN${NETWORK_DOMAIN:-local}
    [[ -z "${NETWORK_GATEWAY}" ]] || echo "You MUST have NETWORK_GATEWAY variable set in your variables file" && exit 1
    NETWORK_HOSTNAME${NETWORK_HOSTNAME:-$HOSTNAME}
    NETWORK_INTERFACE${NETWORK_INTERFACE:-eth0}
    NETWORK_IP_ADDRESS=${XIVO_IP_DATA}
    NETWORK_NAMESERVERS${NETWORK_NAMESERVERS:-127.0.0.1}
    [[ -z "${NETWORK_NETMASK}" ]] || echo "You MUST have NETWORK_NETMASK variable set in your variables file" && exit 1
    TIMEZONE${TIMEZONE:-GMT}
}

passing_wizard(){
echo "{
\"admin_password\": \"${ADMIN_PASSWORD}\",
\"context_incall\": {
    \"did_length\": ${CONTEXT_INCALL_DID_LENGTH},
    \"display_name\": \"${CONTEXT_INCALL_DISPLAY_NAME}\",
    \"number_end\": \"${CONTEXT_INCALL_NUMBER_END}\",
    \"number_start\": \"${CONTEXT_INCALL_NUMBER_START}\"
},
\"context_internal\": {
    \"display_name\": \"${CONTEXT_INTERNAL_DISPLAY_NAME}\",
    \"number_end\": \"${CONTEXT_INCALL_NUMBER_END}\",
    \"number_start\": \"${CONTEXT_INTERNAL_NUMBER_START}\"
},
\"context_outcall\": {
    \"display_name\": \"${CONTEXT_OUTCALL_DISPLAY_NAME}\"
},
\"default_french_configuration\": ${DEFAULT_FRENCH_CONFIGURATION},
\"entity_name\": \"${ENTITY_NAME}\",
\"language\": \"${LANGUAGE}\",
\"license\": true,
\"network\": {
    \"domain\": \"${NETWORK_DOMAIN}\",
    \"gateway\": \"${NETWORK_GATEWAY}\",
    \"hostname\": \"${NETWORK_HOSTNAME}\",
    \"interface\": \"${NETWORK_INTERFACE}\",
    \"ip_address\": \"${NETWORK_IP_ADDRESS}\",
    \"nameservers\": [
    \"${NETWORK_NAMESERVERS}\"
    ],
    \"netmask\": \"${NETWORK_NETMASK}\"
},
\"timezone\": \"${TIMEZONE}\"
}" > /tmp/api.json

echo 'Sending the JSON via cURL ...'
curl -X POST -H 'Content-Type: application/json'  -H "Accept: application/json" -d @/tmp/api.json --insecure "https://${XIVO_FQDN}:9486/1.1/wizard"
}

do_install_rabbitmq_container=0
do_add_pgdg_mirror=0

get_system_architecture

user_has_been_asked_before=0
if [ -z $1 ] && [ "$architecture" != "x86_64" ]; then
    if ! check_distribution_is_32bit; then
        propose_polaris_installation
    fi
else
    if [[ $# -eq 0 ]]; then
        ask_before_proceeding_installation
        default_lts_preinstallation
    fi
    while getopts ':dsra:' opt; do
        case ${opt} in
            d)
                ask_before_proceeding_installation -d
                check_system_is_64bit
                do_add_pgdg_mirror=1
                distribution="xivo-$dev_lts-dev"
                debian_codename=$debian_codename_dev
                debian_version=$debian_version_dev
                do_install_rabbitmq_container=1
                ;;
            s)
                source_variables_file
                configure_reply=true
                ;;
            r)
                echo "xivo-rc distribution does not exist anymore"
                echo "use option -a VERSION (e.g. -a 2018.14.00) to install a RC"
                exit 1
                ;;
            a)
                check_archive_prefix $OPTARG
                ask_before_proceeding_installation -a $OPTARG
                distribution="xivo-$OPTARG"
                repo='archive'

                if [ "$OPTARG" \> "2018" ]; then
                    check_system_is_64bit
                fi
                if [ "${OPTARG::7}" = "2018.02" ]; then
                    add_docker-engine_key
                fi

                if dpkg --compare-versions "$OPTARG" ">=" "2020.01.00"; then
                    do_install_rabbitmq_container=1
                fi

                if dpkg --compare-versions "$OPTARG" "ge" "2022.04.00"; then
                    debian_version=$debian_version
                    debian_codename=$debian_codename
                    do_add_pgdg_mirror=1
                elif [ "$OPTARG" \> "2020.09" ]; then
                    debian_version='10'
                    debian_codename='buster'
                    do_add_pgdg_mirror=1
                elif [ "$OPTARG" \> "2018.13" ]; then
                    debian_version='9'
                    debian_codename='stretch'
                    do_add_pgdg_mirror=1
                elif [ "$OPTARG" \> "15.19" ]; then
                    debian_version='8'
                    debian_codename='jessie'
                elif [ "$OPTARG" \> "14.17" ]; then
                    debian_version='7'
                    debian_codename='wheezy'
                else
                    # 14.17 and earlier don't have xivo-dist available
                    echo "This script only supports installing XiVO 14.18 or later."
                    exit 1
                fi
                ;;
            *)
                usage
                ;;
        esac
    done
fi
if [[ ${user_has_been_asked_before} -eq 0 ]]; then
    # We need this to prompt the user if he runs script with wrong arg
    # e.g.: ./xivo_install.sh callisto
    ask_before_proceeding_installation $1 $2
fi
recover_from_wrong_distrib_input

check_system

add_xivo_apt_conf
add_dependencies
if [ "${debian_codename}" = "buster" ] || [ "${debian_codename}" = "bullseye" ]; then
    add_backports_in_sources_list ${debian_codename}
fi
if [ $do_add_pgdg_mirror -eq 1 ]; then
    add_pgdg_mirror
fi
add_mirror
install_xivo
postinst_actions

if [[ $configure_reply ]];then
    passing_wizard
    echo "Your XiVO PBX is now install"
fi

